<?php

if (!function_exists('str_replace_first')) {
    /**
     * @param $from
     * @param $to
     * @param $content
     * @return string|string[]|null
     */
    function str_replace_first($from, $to, $content){
        $from = '/'.preg_quote($from, '/').'/';
        return preg_replace($from, $to, $content, 1);
    }
}

if (!function_exists('small_product_url')) {
    /**
     * @param $url
     * @return string
     */
    function small_product_url($url){
        $basename = basename($url);
        $nw_url = str_replace($basename, '', $url);
        $nw_bsnm = str_replace_first('.', '_small.', $basename);
        return $nw_url . $nw_bsnm;
    }
}
