<?php

namespace App\Listeners;

use App\Events\CheckOrder;
use App\Models\Key;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Http;

class AddOrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        \Log::info('debug Listener');
    }

    /**
     * Handle the event.
     *
     * @param  CheckOrder  $event
     * @return void
     */
    public function handle(CheckOrder $event)
    {
        $eventData = $event->data;
        $order_id = $eventData['order_id'];
        $user_id = $eventData['user_id'];

        $shop = User::where('id', $user_id)->first();

        $endPoint = 'admin/api/'.env('SHOPIFY_API_VERSION').'/orders/'.$order_id.'.json';
        $result = $shop->api()->rest('GET', $endPoint);

        if (!$result['errors']) {
            $sh_order = $result['body']->container['order'];
            $key = Key::select('form_key')->where('user_id', $user_id)->first();

            if( $key['form_key'] != '' ){
                $product = [];

                $lineItems = $sh_order['line_items'];
                if (!empty($lineItems)) {
                    foreach ($lineItems as $lkey => $lval) {
                        $endPoint = 'admin/api/'.env('SHOPIFY_API_VERSION').'/products/'.$lval['product_id'].'.json';
                        $parameter['fields'] = 'handle,image';
                        $result = $shop->api()->rest('GET', $endPoint, $parameter);

                        if (!$result['errors']) {
                            $sh_product = $result['body']->container['product'];
                            $p[$lkey]['image'] = (@$sh_product['image']['src']) ? small_product_url($sh_product['image']['src']) : '';
                            $p[$lkey]['productName'] = $lval['title'];
                            $p[$lkey]['productID'] = $lval['product_id'];
                            $p[$lkey]['productURL'] = 'https://'.$shop->name.'/products/'.$sh_product['handle'];
                        }
                    }
                    $product = $p;
                }

                $json = [
                    'apiKey' => $key['form_key'],
                    'integrationID' => 2,
                    'firstName' => (@$sh_order['customer']['first_name']) ? @$sh_order['customer']['first_name'] : '',
                    'ip' => $sh_order['browser_ip'],
                    'purchaseTime' => $sh_order['created_at'],
                    'storeDomain' => $shop['name'],
                    'products' => $product
                ];
                $this->storeOrder($json);
            }
         }
    }

    public function storeOrder($json){
        try{
            \Log::info($json);
            $endPoint = config('const.SOCIAL_ORACLE_ENDPOINT') . 'order';
            $response = Http::withHeaders([
                'Content-Type' => 'application/json'
            ])->asJson()->post($endPoint, $json);
            $res = $response->json();
            \Log::info($res);
        }catch ( \Exception $e ){
            return false;
        }
    }
}
