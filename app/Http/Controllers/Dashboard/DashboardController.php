<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Key;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class DashboardController extends Controller
{
    public function index(){
        try{
            $shop = \Auth::user();
            $key = Key::select('id','form_key')->where('user_id', $shop->id)->first();
            if( !$key ){
                $key['form_key'] = '';
                $key['id'] = '';
            }
            return Response()->json(['data' => $key], 200);
        }catch( \Exception $e ){
            return Response()->json(['data' => $e->getMessage()], 422);
        }
    }
    public function store(Request $request){
        try{
            $shop = \Auth::user();
            $form_data = $request->data;
            if( $form_data['id'] == '' ){
                $json = $this->getJSON($form_data['form_key']);
                if( $json ){
                    $endPoint = config('const.SOCIAL_ORACLE_ENDPOINT') . 'order/bulk';
                    $response = Http::withHeaders([
                        'Content-Type' => 'application/json'
                    ])->asJson()->post($endPoint, $json);
                    $res = $response->json();

                    $msg = 'Key not saved!';
                    if( $res['success'] ){
                        $publicKey = $res['records']['publicKey'];
                        $key = new Key;
                        $key->user_id = $shop->id;
                        $key->form_key = $form_data['form_key'];
                        $key->public_key = $publicKey;
                        $key->save();
                        $msg = 'Key Saved!';

                        $this->addScriptTag($publicKey);
                    }
                }
            }else{
                $key = Key::where('user_id', $shop->id)->where('id', $form_data['id'])->first();
                $msg = 'Key not saved!';
                if( $key ){
                    $key->form_key = $form_data['form_key'];
                    $key->save();
                    $msg = 'Key Saved!';
                }
            }
            return Response()->json(['data' => $msg], 200);
        }catch( \Exception $e ){
            return Response()->json(['data' => $e->getMessage()], 422);
        }
    }

    public function getJSON($apiKey){
        try{
            $shop = \Auth::user();
            $order = [];

            $endPoint = 'admin/api/' . env('SHOPIFY_API_VERSION') . '/orders.json';
            $parameter['limit'] = 30;
            $parameter['fields'] = 'customer,browser_ip,created_at,line_items';
            $result = $shop->api()->rest('GET', $endPoint, $parameter);
            if( !$result['errors'] ){
                $sh_orders = $result['body']->container['orders'];
                if( !empty($sh_orders) ){
                    foreach ( $sh_orders as $key=>$val ){
                        $o['firstName'] = (@$val['customer']['first_name']) ? @$val['customer']['first_name'] : '';
                        $o['ip'] = $val['browser_ip'];
                        $o['purchaseTime'] = $val['created_at'];
                        $lineItems = $val['line_items'];
                        if( !empty($lineItems) ){
                            foreach ( $lineItems as $lkey=>$lval ){
                                $endPoint = 'admin/api/' . env('SHOPIFY_API_VERSION') . '/products/'.$lval['product_id'].'.json';
                                $parameter['fields'] = 'handle,image';
                                $result = $shop->api()->rest('GET', $endPoint, $parameter);

                                if( !$result['errors'] ){
                                    $sh_product = $result['body']->container['product'];
                                    $p[$lkey]['image'] = (@$sh_product['image']['src']) ? small_product_url($sh_product['image']['src']) : '';
                                    $p[$lkey]['productName'] = $lval['title'];
                                    $p[$lkey]['productID'] = $lval['product_id'];
                                    $p[$lkey]['productURL'] = 'https://' . $shop->name . '/products/' . $sh_product['handle'];
                                }
                            }
                        }
                        $o['products'] = $p;
                        array_push($order, $o);
                    }
                }
            }

            $json = [
                'apiKey' => $apiKey,
                'integrationID' => 2,
                'storeDomain' => $shop->name,
                'setup' => true,
                'orders' => $order,
            ];
            return $json;
        }catch(\Exception $e){
            return false;
        }
    }

    function addScriptTag($publicKey){
        try {
            \Log::info('-----------------------START :: addScriptCode -----------------------');
            $shop = \Auth::user();
            $js = config('const.SCRIPT_CODE_ENDPOINT') .'?key='. $publicKey;
            $json = [
                'script_tag' => [
                    "event"=> "onload",
                    "src" => $js,
                    "id" => "smp-script"
                ]
            ];
            $endPoint = 'admin/api/' . env('SHOPIFY_API_VERSION'). '/script_tags.json';
            $result = $shop->api()->rest('post', $endPoint, $json );
        } catch (\Exception $e) {
           dd($e);
        }
    }
}
