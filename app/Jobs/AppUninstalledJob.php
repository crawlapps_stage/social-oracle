<?php

namespace App\Jobs;

use App\Models\Key;
use App\User;
use Osiset\ShopifyApp\Actions\CancelCurrentPlan;
use Osiset\ShopifyApp\Contracts\Commands\Shop as IShopCommand;
use Osiset\ShopifyApp\Contracts\Queries\Shop as IShopQuery;

class AppUninstalledJob extends \Osiset\ShopifyApp\Messaging\Jobs\AppUninstalledJob
{
    /**
    * Execute the job.
    *
    * @return bool
    */
    public function handle(
        IShopCommand $shopCommand,
        IShopQuery $shopQuery,
        CancelCurrentPlan $cancelCurrentPlanAction
    ): bool {
        \Log::info('------------------- App uninstalled JOB-------------------');
        // Get the shop
        $shop = $shopQuery->getByDomain($this->domain);
        $shopId = $shop->getId();

        // Cancel the current plan
        $cancelCurrentPlanAction($shopId);
        $shop = User::where('name', $shop->name)->first();
        $key = Key::where('user_id', $shop['id'])->first();
        (@$key) ? $key->delete() : '';
        $shop->password = '';
        $shop->save();
        // Purge shop of token, plan, etc.
        $shopCommand->clean($shopId);

        // Soft delete the shop.
        $shopCommand->softDelete($shopId);
        return true;
    }
}
