<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Global constants
    |--------------------------------------------------------------------------
    */

    'SOCIAL_ORACLE_ENDPOINT' => 'https://www.smp.socialoracle.app/api/campaigns/shopify/',

    'SCRIPT_CODE_ENDPOINT' => 'https://cdn.socialoracle.app/smp.js',

];
